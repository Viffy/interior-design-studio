CREATE DATABASE InteriorDesignStudio;
USE InteriorDesignStudio
CREATE TABLE Users (
    UserID INT PRIMARY KEY IDENTITY(1,1),
    Name NVARCHAR(50),
    Surname NVARCHAR(50),
    Email NVARCHAR(100),
    Password NVARCHAR(50),
    Phone NVARCHAR(20),
    Address NVARCHAR(255),
    UserType NVARCHAR(10) CHECK (UserType IN ('Client', 'Designer','Admin'))
);


CREATE TABLE Projects (
    ProjectID INT PRIMARY KEY IDENTITY(1,1),
    ProjectName NVARCHAR(100),
    CreationDate DATE,
    Description NVARCHAR(MAX),
    ImagePath NVARCHAR(MAX),
    UserID INT FOREIGN KEY REFERENCES Users(UserID)
);

CREATE TABLE Services (
    ServiceID INT PRIMARY KEY IDENTITY(1,1),
    ServiceName NVARCHAR(100),
    Description NVARCHAR(MAX),
    Cost DECIMAL(10, 2)
);


CREATE TABLE WorkflowStages (
    StageID INT PRIMARY KEY IDENTITY(1,1),
    StageName NVARCHAR(100)
);


CREATE TABLE Comments (
    CommentID INT PRIMARY KEY IDENTITY(1,1),
    CommentText NVARCHAR(MAX),
    CreationDate DATETIME,
    UserID INT FOREIGN KEY REFERENCES Users(UserID),
    ProjectID INT FOREIGN KEY REFERENCES Projects(ProjectID)
);


CREATE TABLE Orders (
    OrderID INT PRIMARY KEY IDENTITY(1,1),
    UserID INT FOREIGN KEY REFERENCES Users(UserID),
    ServiceID INT FOREIGN KEY REFERENCES Services(ServiceID),
    StageID INT FOREIGN KEY REFERENCES WorkflowStages(StageID),
    OrderDate DATE,
    ProjectName NVARCHAR(100),
    Description NVARCHAR(MAX)
);

INSERT INTO WorkflowStages (StageName) VALUES 
('Initial'),
('Design'),
('Approval'),
('Implementation'),
('Completion');

INSERT INTO Services (ServiceName, Description, Cost) VALUES 
('Interior Design', 'Comprehensive interior design services', 1500.00),
('Furniture Selection', 'Assistance in selecting and sourcing furniture', 800.00),
('Color Consultation', 'Help with choosing color schemes for spaces', 500.00),
('Space Planning', 'Optimizing the layout of interior spaces', 1200.00),
('Lighting Design', 'Design and selection of lighting fixtures', 1000.00);
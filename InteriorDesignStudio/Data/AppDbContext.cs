﻿using InteriorDesignStudio.Models.Domain;
using Microsoft.EntityFrameworkCore;

namespace InteriorDesignStudio.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions options) : base(options)
        {

        }
        public DbSet<User> Users { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<WorkflowStage> WorkflowStages { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Order> Orders { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasKey(u => u.UserID);
            modelBuilder.Entity<User>()
                .Property(u => u.UserType)
                .IsRequired()
                .HasMaxLength(10);

            modelBuilder.Entity<Project>()
                .Property(p => p.ProjectName)
                .IsRequired()
                .HasMaxLength(100);
            modelBuilder.Entity<Project>()
                .HasOne(p => p.User)
                .WithMany(u => u.Projects)
                .HasForeignKey(p => p.UserID);

            modelBuilder.Entity<Service>()
                .HasKey(s => s.ServiceID);
            modelBuilder.Entity<Service>()
                .Property(s => s.ServiceName)
                .IsRequired()
                .HasMaxLength(100);

            modelBuilder.Entity<WorkflowStage>()
                .HasKey(ws => ws.StageID);
            modelBuilder.Entity<WorkflowStage>()
                .Property(ws => ws.StageName)
                .IsRequired()
                .HasMaxLength(100);

            modelBuilder.Entity<Comment>()
                .HasKey(c => c.CommentID);
            modelBuilder.Entity<Comment>()
                .Property(c => c.CommentText)
                .IsRequired()
                .HasMaxLength(int.MaxValue);
            modelBuilder.Entity<Comment>()
                .HasOne(c => c.User)
                .WithMany(u => u.Comments)
                .HasForeignKey(c => c.UserID);
            modelBuilder.Entity<Comment>()
                .HasOne(c => c.Project)
                .WithMany(p => p.Comments)
                .HasForeignKey(c => c.ProjectID);

            modelBuilder.Entity<Order>()
                .HasKey(o => o.OrderID);
            modelBuilder.Entity<Order>()
                .HasOne(o => o.User)
                .WithMany(u => u.Orders)
                .HasForeignKey(o => o.UserID);
            modelBuilder.Entity<Order>()
                .HasOne(o => o.Service)
                .WithMany(s => s.Orders)
                .HasForeignKey(o => o.ServiceID);
            modelBuilder.Entity<Order>()
                .HasOne(o => o.WorkflowStage)
                .WithMany(ws => ws.Orders)
                .HasForeignKey(o => o.StageID);
        }
    }
}

﻿namespace InteriorDesignStudio.Repository.Interfaces
{
    public interface IStatisticsRepository
    {
        Task<int> GetUserProjectCountAsync(int userId);
        Task<int> GetUserOrderCountAsync(int userId);
        Task<IEnumerable<(string ServiceName, int OrderCount)>> GetServiceOrderCountsAsync();
        Task<IEnumerable<(DateTime Date, int ProjectCount)>> GetDailyProjectCountsAsync();
    }
}

﻿using InteriorDesignStudio.Models.Domain;

namespace InteriorDesignStudio.Repository.Interfaces
{
    public interface IWorkflowStageRepository
    {
        Task<IEnumerable<WorkflowStage>> GetAllStagesAsync();
        Task<WorkflowStage> GetStageByIdAsync(int stageId);
        Task AddStageAsync(WorkflowStage stage);
        Task UpdateStageAsync(WorkflowStage stage);
        Task DeleteStageAsync(int stageId);
    }
}

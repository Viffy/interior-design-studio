﻿using InteriorDesignStudio.Data;
using InteriorDesignStudio.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace InteriorDesignStudio.Repository.Implementations
{
    public class StatisticsRepository : IStatisticsRepository
    {
        private readonly AppDbContext _context;

        public StatisticsRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<int> GetUserProjectCountAsync(int userId)
        {
            return await _context.Projects.CountAsync(p => p.UserID == userId);
        }

        public async Task<int> GetUserOrderCountAsync(int userId)
        {
            return await _context.Orders.CountAsync(o => o.UserID == userId);
        }

        public async Task<IEnumerable<(string ServiceName, int OrderCount)>> GetServiceOrderCountsAsync()
        {
            var result = await _context.Services
                .Select(s => new
                {
                    ServiceName = s.ServiceName,
                    OrderCount = s.Orders.Count()
                })
                .ToListAsync();

            return result.Select(r => (r.ServiceName, r.OrderCount));
        }

        public async Task<IEnumerable<(DateTime Date, int ProjectCount)>> GetDailyProjectCountsAsync()
        {
            var result = await _context.Projects
                .GroupBy(p => p.CreationDate.Date)
                .Select(g => new
                {
                    Date = g.Key,
                    ProjectCount = g.Count()
                })
                .ToListAsync();

            return result.Select(r => (r.Date, r.ProjectCount));
        }

    }

}

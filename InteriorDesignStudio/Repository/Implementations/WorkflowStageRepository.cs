﻿using InteriorDesignStudio.Data;
using InteriorDesignStudio.Models.Domain;
using InteriorDesignStudio.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace InteriorDesignStudio.Repository.Implementations
{
    public class WorkflowStageRepository : IWorkflowStageRepository
    {
        private readonly AppDbContext _context;

        public WorkflowStageRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<WorkflowStage>> GetAllStagesAsync()
        {
            return await _context.WorkflowStages.ToListAsync();
        }

        public async Task<WorkflowStage> GetStageByIdAsync(int stageId)
        {
            return await _context.WorkflowStages.FindAsync(stageId);
        }

        public async Task AddStageAsync(WorkflowStage stage)
        {
            _context.WorkflowStages.Add(stage);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateStageAsync(WorkflowStage stage)
        {
            _context.Entry(stage).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task DeleteStageAsync(int stageId)
        {
            var stage = await _context.WorkflowStages.FindAsync(stageId);

            if (stage != null)
            {
                _context.WorkflowStages.Remove(stage);
                await _context.SaveChangesAsync();
            }
        }
    }
}

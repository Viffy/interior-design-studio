﻿namespace InteriorDesignStudio.Models.Domain
{
    public class Order
    {
        public int OrderID { get; set; }
        public DateTime OrderDate { get; set; }
        public string ProjectName { get; set; }
        public string Description { get; set; }
        public int UserID { get; set; }
        public int ServiceID { get; set; }
        public int StageID { get; set; }
        public User User { get; set; }
        public Service Service { get; set; }
        public WorkflowStage WorkflowStage { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace InteriorDesignStudio.Models.Domain
{
    public class User
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string? UserType { get; set; }

        public ICollection<Comment>? Comments { get; set; }
        public ICollection<Project>? Projects { get; set; }
        public ICollection<Order>? Orders { get; set; }
    }

}

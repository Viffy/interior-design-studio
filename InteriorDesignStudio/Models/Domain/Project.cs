﻿namespace InteriorDesignStudio.Models.Domain
{
    public class Project
    {
        public int ProjectID { get; set; }
        public string ProjectName { get; set; }
        public DateTime CreationDate { get; set; }
        public string Description { get; set; }
        public string ImagePath { get; set; }
        public int UserID { get; set; }
        public User User { get; set; }
        public ICollection<Comment>? Comments { get; set; }
    }
}

﻿namespace InteriorDesignStudio.Models.Domain
{
    public class Comment
    {
        public int CommentID { get; set; }
        public string CommentText { get; set; }
        public DateTime CreationDate { get; set; }

        public int UserID { get; set; }
        public User User { get; set; }
        public int ProjectID { get; set; }
        public Project Project { get; set; }
    }
}

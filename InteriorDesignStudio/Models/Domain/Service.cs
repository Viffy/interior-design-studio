﻿namespace InteriorDesignStudio.Models.Domain
{
    public class Service
    {
        public int ServiceID { get; set; }
        public string ServiceName { get; set; }
        public string Description { get; set; }
        public decimal Cost { get; set; }

        public ICollection<Order> Orders { get; set; }
    }
}

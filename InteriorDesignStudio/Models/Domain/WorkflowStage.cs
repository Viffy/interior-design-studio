﻿namespace InteriorDesignStudio.Models.Domain
{
    public class WorkflowStage
    {
        public int StageID { get; set; }
        public string StageName { get; set; }
        public ICollection<Order> Orders { get; set; }
    }

}

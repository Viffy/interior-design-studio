﻿namespace InteriorDesignStudio.ViewModels
{
    public class WorkflowStageDeleteViewModel
    {
        public int StageID { get; set; }
        public string StageName { get; set; }
    }
}

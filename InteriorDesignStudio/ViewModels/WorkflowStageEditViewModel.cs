﻿using System.ComponentModel.DataAnnotations;

namespace InteriorDesignStudio.ViewModels
{
    public class WorkflowStageEditViewModel
    {
        public int StageID { get; set; }

        [Required(ErrorMessage = "Stage Name is required")]
        [StringLength(100, ErrorMessage = "Stage Name must be at most 100 characters")]
        public string StageName { get; set; }
    }
}

﻿namespace InteriorDesignStudio.ViewModels
{
    public class ProjectCreateViewModel
    {
        public string ProjectName { get; set; }
        public DateTime CreationDate { get; set; }
        public string Description { get; set; }
        public IFormFile ImageData { get; set; }
    }
}

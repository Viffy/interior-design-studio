﻿using InteriorDesignStudio.Models.Domain;

namespace InteriorDesignStudio.ViewModels
{
    public class AdminStatisticsViewModel
    {
        public IEnumerable<(string ServiceName, int OrderCount)> ServiceOrderCounts { get; set; }
        public IEnumerable<(DateTime Date, int ProjectCount)> DailyProjectCounts { get; set; }
        public IEnumerable<User> Users { get; set; }
    }
}

﻿namespace InteriorDesignStudio.ViewModels
{
    public class ProjectEditViewModel
    {
        public int ProjectID { get; set; }
        public string ProjectName { get; set; }
        public DateTime CreationDate { get; set; }
        public string Description { get; set; }
        public int UserID { get; set; }
    }
}

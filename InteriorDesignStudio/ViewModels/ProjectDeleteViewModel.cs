﻿using System.ComponentModel.DataAnnotations;

namespace InteriorDesignStudio.ViewModels
{
    public class ProjectDeleteViewModel
    {
        public int ProjectID { get; set; }
        public string? ProjectName { get; set; }
        public DateTime CreationDate { get; set; }
        public string? Description { get; set; }
        public string? ImagePath { get; set; }
        public int? UserID { get; set; } 

        [Required(ErrorMessage = "Please enter your password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}

﻿using InteriorDesignStudio.Models.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations;

namespace InteriorDesignStudio.ViewModels
{
    public class CommentCreateViewModel
    {
        public int ProjectID { get; set; }
        [Required(ErrorMessage = "Please enter a comment.")]
        public string CommentText { get; set; }

    }
}

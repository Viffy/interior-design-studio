﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace InteriorDesignStudio.ViewModels
{
    public class OrderChangeStatusViewModel
    {
        public int OrderID { get; set; }
        public string ProjectName { get; set; }
        public string Description { get; set; }
        public int SelectedStageID { get; set; }
        public IEnumerable<SelectListItem>? StageOptions { get; set; }
    }
}

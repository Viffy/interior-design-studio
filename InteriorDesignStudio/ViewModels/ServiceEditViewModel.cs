﻿using System.ComponentModel.DataAnnotations;

namespace InteriorDesignStudio.ViewModels
{
    public class ServiceEditViewModel
    {
        public int ServiceID { get; set; }

        [Required(ErrorMessage = "Service Name is required")]
        [StringLength(100, ErrorMessage = "Service Name must be at most 100 characters")]
        public string ServiceName { get; set; }

        [Required(ErrorMessage = "Description is required")]
        [MaxLength(int.MaxValue, ErrorMessage = "Description is too long")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Cost is required")]
        [Range(0, double.MaxValue, ErrorMessage = "Cost must be a non-negative number")]
        public decimal Cost { get; set; }
    }
}

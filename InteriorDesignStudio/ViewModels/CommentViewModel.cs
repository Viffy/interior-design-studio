﻿namespace InteriorDesignStudio.ViewModels
{
    public class CommentViewModel
    {
        public int CommentId { get; set; }
        public string CommentText { get; set; }
    }
}

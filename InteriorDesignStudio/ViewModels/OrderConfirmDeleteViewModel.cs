﻿namespace InteriorDesignStudio.ViewModels
{
    public class OrderConfirmDeleteViewModel
    {
        public int OrderID { get; set; }
        public string ProjectName { get; set; }
        public string Description { get; set; }
        public string Password { get; set; }
    }
}

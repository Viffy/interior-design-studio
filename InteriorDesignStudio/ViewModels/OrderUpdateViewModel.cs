﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace InteriorDesignStudio.ViewModels
{
    public class OrderUpdateViewModel
    {
        public int OrderID { get; set; }
        public string ProjectName { get; set; }
        public string Description { get; set; }
        public int SelectedServiceID { get; set; }
        public IEnumerable<SelectListItem>? ServiceOptions { get; set; }
    }
}

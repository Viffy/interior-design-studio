﻿namespace InteriorDesignStudio.ViewModels
{
    public class ServiceDeleteViewModel
    {
        public int ServiceID { get; set; }
        public string ServiceName { get; set; }
        public string Description { get; set; }
        public decimal Cost { get; set; }
    }
}

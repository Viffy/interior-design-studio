﻿using InteriorDesignStudio.Models.Domain;
using InteriorDesignStudio.Repository.Interfaces;
using InteriorDesignStudio.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace InteriorDesignStudio.Controllers
{
    public class WorkflowStageController : Controller
    {
        private readonly IWorkflowStageRepository _workflowStageRepository;

        public WorkflowStageController(IWorkflowStageRepository workflowStageRepository)
        {
            _workflowStageRepository = workflowStageRepository;
        }
        [Authorize(Roles = "Designer,Admin")]
        public async Task<IActionResult> Index()
        {
            var stages = await _workflowStageRepository.GetAllStagesAsync();
            return View(stages);
        }

        [HttpGet]
        [Authorize(Roles = "Designer,Admin")]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(WorkflowStageCreateViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var stage = new WorkflowStage
                {
                    StageName = viewModel.StageName
                };

                await _workflowStageRepository.AddStageAsync(stage);
                return RedirectToAction("Index");
            }

            return View(viewModel);
        }

        [HttpGet]
        [Authorize(Roles = "Designer,Admin")]
        public async Task<IActionResult> Edit(int id)
        {
            var stage = await _workflowStageRepository.GetStageByIdAsync(id);

            if (stage == null)
            {
                return NotFound();
            }

            var viewModel = new WorkflowStageEditViewModel
            {
                StageID = stage.StageID,
                StageName = stage.StageName
            };

            return View(viewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(WorkflowStageEditViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var stage = new WorkflowStage
                {
                    StageID = viewModel.StageID,
                    StageName = viewModel.StageName
                };

                await _workflowStageRepository.UpdateStageAsync(stage);
                return RedirectToAction("Index");
            }

            return View(viewModel);
        }

        [HttpGet]
        [Authorize(Roles = "Designer,Admin")]
        public async Task<IActionResult> Delete(int id)
        {
            var stage = await _workflowStageRepository.GetStageByIdAsync(id);

            if (stage == null)
            {
                return NotFound();
            }

            var viewModel = new WorkflowStageDeleteViewModel
            {
                StageID = stage.StageID,
                StageName = stage.StageName
            };

            return View(viewModel);
        }

        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "Designer,Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _workflowStageRepository.DeleteStageAsync(id);
            return RedirectToAction("Index");
        }
    }
}

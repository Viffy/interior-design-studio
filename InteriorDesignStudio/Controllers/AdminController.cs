﻿using InteriorDesignStudio.Repository.Interfaces;
using InteriorDesignStudio.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace InteriorDesignStudio.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private readonly IStatisticsRepository _statisticsRepository;
        private readonly IUserRepository _userRepository;

        public AdminController(IStatisticsRepository statisticsRepository, IUserRepository userRepository)
        {
            _statisticsRepository = statisticsRepository;
            _userRepository = userRepository;
        }
        public async Task<IActionResult> Index()
        {
            var serviceOrderCounts = await _statisticsRepository.GetServiceOrderCountsAsync();
            var dailyProjectCounts = await _statisticsRepository.GetDailyProjectCountsAsync();
            var allUsers = await _userRepository.GetAllUsersAsync();

            var viewModel = new AdminStatisticsViewModel
            {
                ServiceOrderCounts = serviceOrderCounts,
                DailyProjectCounts = dailyProjectCounts,
                Users = allUsers
            };

            return View(viewModel);
        }
        [HttpPost]
        public async Task<IActionResult> ChangeUserRole(int userId, string newRole)
        {
            var user = await _userRepository.GetUserByIdAsync(userId);

            if (user != null)
            {
                user.UserType = newRole;
                await _userRepository.UpdateUserAsync(user);

                return RedirectToAction("Index", "Admin");
            }

            return NotFound();
        }
    }
}

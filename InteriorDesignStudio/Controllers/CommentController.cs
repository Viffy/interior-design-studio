﻿using InteriorDesignStudio.Models.Domain;
using InteriorDesignStudio.Repository.Interfaces;
using InteriorDesignStudio.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace InteriorDesignStudio.Controllers
{
    public class CommentController : Controller
    {
        private readonly ICommentRepository _commentRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly IUserRepository _userRepository;
        public CommentController(ICommentRepository commentRepository, IProjectRepository projectRepository, IUserRepository userRepository)
        {
            _commentRepository = commentRepository;
            _projectRepository = projectRepository;
            _userRepository = userRepository;
        }

        [HttpGet]
        [Authorize]
        [Route("/comment/create/{projectId}")]
        public async Task<IActionResult> Create(int projectId)
        {
            var project = await _projectRepository.GetProjectByIdAsync(projectId);

            if (project == null)
            {
                return RedirectToAction("Index", "Project");
            }

            var viewModel = new CommentCreateViewModel
            {
                ProjectID = projectId
            };

            return View(viewModel);
        }

        [HttpPost]
        [Authorize]
        [Route("/comment/create/{projectId}")]
        public async Task<IActionResult> Create(int projectId, CommentCreateViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var comment = new Comment
                {
                    CommentText = viewModel.CommentText,
                    CreationDate = DateTime.Now,
                    UserID = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)),
                    ProjectID = projectId
                };

                await _commentRepository.AddCommentAsync(comment);

                return RedirectToAction("Details", "Project", new { id = projectId });
            }

            return View(viewModel);
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Delete(int id)
        {
            var comment = await _commentRepository.GetCommentByIdAsync(id);
            var user = await _userRepository.GetUserByIdAsync(comment.UserID);
            if (user == null)
            {
                return RedirectToAction("Index", "Project");
            }

            if(user.UserID != comment.UserID)
            {
                return RedirectToAction("Index", "Project");
            }

            if(comment == null)
            {
                return RedirectToAction("Index", "Project");
            }
            var viewModel = new CommentViewModel
            {
                CommentId = comment.CommentID,
                CommentText = comment.CommentText,
            };
            return View(viewModel);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> DeletePost(int commentId)
        {
            var comment = await _commentRepository.GetCommentByIdAsync(commentId);
            var user = await _userRepository.GetUserByIdAsync(comment.UserID);
            if (user == null)
            {
                return RedirectToAction("Index", "Project");
            }

            if (user.UserID != comment.UserID)
            {
                return RedirectToAction("Index", "Project");
            }

            await _commentRepository.DeleteCommentAsync(commentId);

            return RedirectToAction("Index", "Project");
        }
    }
}

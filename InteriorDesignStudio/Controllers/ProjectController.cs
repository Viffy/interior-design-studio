﻿using InteriorDesignStudio.Models.Domain;
using InteriorDesignStudio.Repository.Interfaces;
using InteriorDesignStudio.Utils;
using InteriorDesignStudio.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace InteriorDesignStudio.Controllers
{
    public class ProjectController : Controller
    {
        private readonly IProjectRepository _projectRepository;
        private readonly IUserRepository _userRepository;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public ProjectController(IProjectRepository projectRepository, IWebHostEnvironment webHostEnvironment, IUserRepository userRepository)
        {
            _projectRepository = projectRepository;
            _webHostEnvironment = webHostEnvironment;
            _userRepository = userRepository;
        }
        public async Task<IActionResult> Index()
        {
            var projects = await _projectRepository.GetAllProjectsAsync();
            return View(projects);
        }
        public async Task<IActionResult> Details(int id)
        {
            var project = await _projectRepository.GetProjectByIdAsync(id);

            if (project == null)
            {
                return NotFound();
            }

            return View(project);
        }

        [HttpGet]
        [Authorize]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Create(ProjectCreateViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                if (viewModel.ImageData != null && viewModel.ImageData.Length > 0)
                {
                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(viewModel.ImageData.FileName);

                    var filePath = Path.Combine(_webHostEnvironment.WebRootPath, "uploads", fileName);

                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await viewModel.ImageData.CopyToAsync(stream);
                    }
                    var project = new Project
                    {
                        ProjectName = viewModel.ProjectName,
                        CreationDate = viewModel.CreationDate,
                        Description = viewModel.Description,
                        UserID = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)),

                        ImagePath = "/uploads/" + fileName,
                    };

                    await _projectRepository.AddProjectAsync(project);
                }

                return RedirectToAction("Index", "Project");
            }

            return View(viewModel);
        }
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Edit(int id)
        {
            var project = await _projectRepository.GetProjectByIdAsync(id);

            if(project.UserID != int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)))
            {
                return RedirectToAction("Index");
            }

            if (project == null)
            {
                return NotFound();
            }

            var viewModel = new ProjectEditViewModel
            {
                ProjectID = project.ProjectID,
                ProjectName = project.ProjectName,
                CreationDate = project.CreationDate,
                Description = project.Description,
                UserID = project.UserID
            };


            return View(viewModel);
        }
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Edit(ProjectEditViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var existingProject = await _projectRepository.GetProjectByIdAsync(viewModel.ProjectID);

                if (existingProject == null)
                {
                    return NotFound();
                }

                existingProject.ProjectName = viewModel.ProjectName;
                existingProject.CreationDate = viewModel.CreationDate;
                existingProject.Description = viewModel.Description;
                existingProject.UserID = viewModel.UserID;

                await _projectRepository.UpdateProjectAsync(existingProject);

                return RedirectToAction("Index");
            }

            return View(viewModel);
        }
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Delete(int id)
        {
            var project = await _projectRepository.GetProjectByIdAsync(id);

            if (project == null)
            {
                return NotFound();
            }

            if (project.UserID != int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)))
            {
                return RedirectToAction("Index","Project");
            }

            var viewModel = new ProjectDeleteViewModel
            {
                ProjectID = project.ProjectID,
                ProjectName = project.ProjectName,
                CreationDate = project.CreationDate,
                Description = project.Description,
                ImagePath = project.ImagePath,
                UserID = project.UserID
            };

            return View(viewModel);
        }
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(ProjectDeleteViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var existingProject = await _projectRepository.GetProjectByIdAsync(viewModel.ProjectID);

                if (existingProject == null)
                {
                    return NotFound();
                }

                var user = await _userRepository.GetUserByIdAsync(existingProject.UserID);

                if (user == null || !PasswordHasher.VerifyPassword(viewModel.Password, user.Password))
                {
                    ModelState.AddModelError("Password", "Invalid password");
                    return View(viewModel);
                }

                await _projectRepository.DeleteProjectAsync(viewModel.ProjectID);

                return RedirectToAction("Index");
            }

            return View(viewModel);
        }

    }
}

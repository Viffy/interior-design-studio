﻿using InteriorDesignStudio.Models.Domain;
using InteriorDesignStudio.Repository.Interfaces;
using InteriorDesignStudio.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace InteriorDesignStudio.Controllers
{
    public class ServiceController : Controller
    {
        private readonly IServiceRepository _serviceRepository;

        public ServiceController(IServiceRepository serviceRepository)
        {
            _serviceRepository = serviceRepository;
        }

        public async Task<IActionResult> Index()
        {
            var services = await _serviceRepository.GetAllServicesAsync();

            if (services != null && services.Any())
            {
                return View(services);
            }
            else
            {
                ViewBag.Message = "No services available.";
                return View();
            }
        }

        [HttpGet]
        [Authorize(Roles = "Designer,Admin")]
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(ServiceCreateViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var service = new Service
                {
                    ServiceName = viewModel.ServiceName,
                    Description = viewModel.Description,
                    Cost = viewModel.Cost
                };

                await _serviceRepository.AddServiceAsync(service);
                return RedirectToAction("Index");
            }

            return View(viewModel);
        }
        [HttpGet]
        [Authorize(Roles = "Designer,Admin")]
        public async Task<IActionResult> Edit(int id)
        {
            var service = await _serviceRepository.GetServiceByIdAsync(id);

            if (service == null)
            {
                return NotFound(); 
            }

            var viewModel = new ServiceEditViewModel
            {
                ServiceID = service.ServiceID,
                ServiceName = service.ServiceName,
                Description = service.Description,
                Cost = service.Cost
            };

            return View(viewModel);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(ServiceEditViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var service = new Service
                {
                    ServiceID = viewModel.ServiceID,
                    ServiceName = viewModel.ServiceName,
                    Description = viewModel.Description,
                    Cost = viewModel.Cost
                };

                await _serviceRepository.UpdateServiceAsync(service);
                return RedirectToAction("Index");
            }

            return View(viewModel);
        }
        [HttpGet]
        [Authorize(Roles = "Designer,Admin")]
        public async Task<IActionResult> Delete(int id)
        {
            var service = await _serviceRepository.GetServiceByIdAsync(id);

            if (service == null)
            {
                return NotFound();
            }

            var viewModel = new ServiceDeleteViewModel
            {
                ServiceID = service.ServiceID,
                ServiceName = service.ServiceName,
                Description = service.Description,
                Cost = service.Cost
            };

            return View(viewModel);
        }
        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "Designer,Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _serviceRepository.DeleteServiceAsync(id);
            return RedirectToAction("Index");
        }
    }
}

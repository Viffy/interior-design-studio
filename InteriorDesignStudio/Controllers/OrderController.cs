﻿using InteriorDesignStudio.Models.Domain;
using InteriorDesignStudio.Repository.Interfaces;
using InteriorDesignStudio.Utils;
using InteriorDesignStudio.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Security.Claims;

namespace InteriorDesignStudio.Controllers
{
    public class OrderController : Controller
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IServiceRepository _serviceRepository;
        private readonly IUserRepository _userRepository;
        private readonly IWorkflowStageRepository _workflowStageRepository;
        public OrderController(IOrderRepository orderRepository, IServiceRepository serviceRepository, IUserRepository userRepository, IWorkflowStageRepository workflowStageRepository)
        {
            _orderRepository = orderRepository;
            _serviceRepository = serviceRepository;
            _userRepository = userRepository;    
            _workflowStageRepository = workflowStageRepository;
        }
        [Authorize(Roles = "Designer,Admin")]
        public async Task<IActionResult> Index()
        {
            var orders = await _orderRepository.GetAllOrdersAsync();
            return View(orders);
        }
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Create()
        {
            var services = await _serviceRepository.GetAllServicesAsync();
            var serviceOptions = services.Select(s => new SelectListItem
            {
                Value = s.ServiceID.ToString(),
                Text = s.ServiceName
            });

            var viewModel = new OrderCreateViewModel
            {
                ServiceOptions = serviceOptions
            };

            return View(viewModel);
        }
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Create(OrderCreateViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var order = new Order
                {
                    ProjectName = viewModel.ProjectName,
                    Description = viewModel.Description,
                    OrderDate = DateTime.Now, 
                    UserID = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)),
                    ServiceID = viewModel.SelectedServiceID, 
                    StageID = 1
                };

                await _orderRepository.AddOrderAsync(order);

                return RedirectToAction("Profile","Account");
            }

            return View(viewModel);
        }
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Edit(int id)
        {
            var order = await _orderRepository.GetOrderByIdAsync(id);
            var user = await _userRepository.GetUserByIdAsync(int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)));

            if (order == null)
            {
                return NotFound();
            }

            if(user == null)
            {
                return NotFound();
            }

            if(user.UserID != order.UserID)
            {
                return RedirectToAction("Profile", "Account");
            }

            var services = await _serviceRepository.GetAllServicesAsync();
            var serviceOptions = services.Select(s => new SelectListItem
            {
                Value = s.ServiceID.ToString(),
                Text = s.ServiceName,
                Selected = s.ServiceID == order.ServiceID
            });

            var viewModel = new OrderUpdateViewModel
            {
                OrderID = order.OrderID,
                ProjectName = order.ProjectName,
                Description = order.Description,
                SelectedServiceID = order.ServiceID,
                ServiceOptions = serviceOptions
            };

            return View(viewModel);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Edit(OrderUpdateViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var order = await _orderRepository.GetOrderByIdAsync(viewModel.OrderID);

                if (order == null)
                {
                    return NotFound();
                }

                order.ProjectName = viewModel.ProjectName;
                order.Description = viewModel.Description;
                order.ServiceID = viewModel.SelectedServiceID;

                await _orderRepository.UpdateOrderAsync(order);

                return RedirectToAction("Profile", "Account");
            }

            return View(viewModel);
        }
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> ConfirmDelete(int id)
        {
            var order = await _orderRepository.GetOrderByIdAsync(id);
            var user = await _userRepository.GetUserByIdAsync(int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)));

            if (user == null)
            { 
                return NotFound(); 
            }

            if (order == null)
            {
                return RedirectToAction("Profile", "Account");
            }

            if(user.UserID != order.UserID)
            {
                return RedirectToAction("Profile", "Account");
            }

            var viewModel = new OrderConfirmDeleteViewModel
            {
                OrderID = order.OrderID,
                ProjectName = order.ProjectName,
                Description = order.Description
            };

            return View(viewModel);
        }
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Delete(int id, OrderConfirmDeleteViewModel viewModel)
        {
            var user = await _userRepository.GetUserByIdAsync(int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)));
            if (user == null)
            {
                return RedirectToAction("Profile", "Account");
            }

            if(PasswordHasher.VerifyPassword(viewModel.Password, user.Password))
            {
                try
                {
                    await _orderRepository.DeleteOrderAsync(id);
                    return RedirectToAction("Profile", "Account");
                }
                catch (Exception ex)
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            return RedirectToAction("Profile", "Account");
        }
        [HttpGet]
        [Authorize(Roles = "Designer,Admin")]
        public async Task<IActionResult> ChangeStatus(int id)
        {
            var order = await _orderRepository.GetOrderByIdAsync(id);
            var workflowStage = await _workflowStageRepository.GetAllStagesAsync();

            if (order == null)
            {
                return NotFound();
            }

            var serviceOptions = workflowStage.Select(wf => new SelectListItem
            {
                Value = wf.StageID.ToString(),
                Text = wf.StageName,
                Selected = wf.StageID == order.StageID
            });

            var viewModel = new OrderChangeStatusViewModel
            {
                OrderID = order.OrderID,
                ProjectName = order.ProjectName,
                Description = order.Description,
                StageOptions = serviceOptions,
            };

            return View(viewModel);
        }
        [HttpPost]
        public async Task<IActionResult> ChangeStatus(OrderChangeStatusViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var order = await _orderRepository.GetOrderByIdAsync(viewModel.OrderID);

                if (order == null)
                {
                    return NotFound();
                }

                order.StageID = viewModel.SelectedStageID;

                await _orderRepository.UpdateOrderAsync(order);

                return RedirectToAction("Index", "Home");
            }

            return View(viewModel);
        }
    }
}
